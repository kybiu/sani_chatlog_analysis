import os
import json
import aiohttp
import asyncio
import argparse
import cloudscraper
import pandas as pd

from sys import platform
from datetime import datetime
from dotenv import load_dotenv
from tqdm import tqdm

load_dotenv()

SANI_ID = os.environ.get("SANI_ID")
LIMIT = os.environ.get("LIMIT")
CONV_FIELDS = os.environ.get("CONV_FIELDS")
MESSAGE_FIELDS = os.environ.get("MESSAGE_FIELDS")
ACCESS_TOKEN = os.environ.get("ACCESS_TOKEN")

bar = ...
done = 0


def get_conversation_ids(from_date=None) -> list:
    try:
        url = "https://graph.facebook.com/v13.0/{sani_id}/conversations?pretty=0&limit={limit}&access_token={access_token}". \
            format(sani_id=SANI_ID, limit=LIMIT, access_token=ACCESS_TOKEN)
        scraper = cloudscraper.create_scraper()
        conv_ids = []
        while True:
            raw_response_text = scraper.get(url).text
            conversations = json.loads(raw_response_text)
            conversations_data = conversations['data']
            conversations_data_ids = [x["id"] for x in conversations_data]

            if from_date:
                if datetime.strptime(from_date, "%d/%m/%Y") > datetime.strptime(
                        conversations['data'][0]['updated_time'][:10], "%Y-%m-%d"):
                    break

            conv_ids = conv_ids + conversations_data_ids

            if "paging" in conversations and any("next" in conversations[x] for x in conversations):
                conversations_paging_next = conversations['paging']['next']
                url = conversations_paging_next
            else:
                break
        return conv_ids
    except Exception as e:
        print(e)
        a = 0


async def get_conversation_detail(conversation_ids, output_dir="chatlog", from_date=None, max_worker: int = 4, db_conn=None):
    queue = asyncio.Queue()
    for c in conversation_ids:
        print('pushed {}'.format(c))
        queue.put_nowait(c)

    tasks = []
    for i in range(max_worker):
        task = asyncio.create_task(get_conversation(f'worker-{i}', from_date, queue, output_dir, db_conn))
        tasks.append(task)

    await queue.join()

    for task in tasks:
        task.cancel()
    await asyncio.gather(*tasks, return_exceptions=True)

    return True


async def get_conversation(name: str, from_date, queue: asyncio.Queue, output_dir="chatlog", db_conn=None):
    try:

        print("Process: " + name)
        global bar
        global done
        while True:
            conversation_id = await queue.get()
            async with aiohttp.ClientSession() as session:
                all_messages_list = []
                url = "https://graph.facebook.com/v13.0/{conversation_id}?fields=" \
                      "senders," \
                      "message_count," \
                      "is_subscribed," \
                      "link,updated_time," \
                      "messages%7Bcreated_time,from,message,to%7D&access_token={access_token}" \
                    .format(conversation_id=conversation_id, access_token=ACCESS_TOKEN)

                conversation_fields_list = ['message_count', 'is_subscribed', 'link', 'updated_time']
                updated_time = ""
                message_count = ""
                is_subscribed = ""
                link = ""
                previous_next = ""

                while True:
                    response = await session.get(url)
                    raw_response_text = await response.text()
                    conversation_detail = json.loads(raw_response_text)

                    date_limit_reached = False
                    messages_paging_next = None

                    if all(name in conversation_detail for name in conversation_fields_list):
                        updated_time = conversation_detail['updated_time']
                        message_count = conversation_detail['message_count']
                        is_subscribed = conversation_detail['is_subscribed']
                        link = conversation_detail['is_subscribed']

                    if "messages" in conversation_detail:
                        messages = conversation_detail['messages']
                        messages_data = messages["data"]
                        if "paging" in messages and any("next" in messages[x] for x in messages):
                            messages_paging_next = messages['paging']['next']
                    else:
                        messages_data = conversation_detail["data"]
                        if "paging" in conversation_detail and any("next" in conversation_detail[x] for x in conversation_detail):
                            messages_paging_next = conversation_detail['paging']['next']

                    for message in messages_data:
                        message_id = message['id']
                        created_time = message['created_time']

                        if from_date:
                            if datetime.strptime(from_date, "%d/%m/%Y") > datetime.strptime(created_time[:10], "%Y-%m-%d"):
                                date_limit_reached = True
                                break

                        content = message['message']
                        from_name = message['from']["name"]
                        to_name = message['to']["data"][0]["name"]
                        from_id = message['from']["id"]
                        to_id = message['to']["data"][0]["id"]

                        message_dict = {
                            "conversation_id": conversation_id,
                            "message_id": message_id,
                            "updated_time": updated_time,
                            "created_time": created_time,
                            "content": content,
                            "from_name": from_name,
                            "to_name": to_name,
                            "from_id": from_id,
                            "to_id": to_id,
                            "message_count": message_count,
                            "is_subscribed": is_subscribed,
                            "link": link,
                        }

                        all_messages_list.append(message_dict)

                    if not messages_paging_next or (messages_paging_next and messages_paging_next == previous_next) or date_limit_reached:
                        break
                    else:
                        previous_next = messages_paging_next
                        url = messages_paging_next

            messages_df = pd.DataFrame(all_messages_list)
            if len(messages_df) > 0:
                if db_conn:
                    query_date = from_date.split("/")[2] + "-" + from_date.split("/")[1] + "-" + from_date.split("/")[0]
                    df_in_db = pd.read_sql("""
                            select message_id
                            from raw_chatlog_fb
                            where conversation_id = \'{}\'
                            and created_time >= \'{}\'
                        """.format(messages_df["conversation_id"].values[0], query_date), db_conn)
                    existed_message_id = df_in_db["message_id"].tolist()
                    messages_df = messages_df[~(messages_df["message_id"].isin(existed_message_id))]
                    messages_df.to_sql('raw_chatlog_fb', db_conn, if_exists='append')
                else:
                    messages_df.to_feather("{}/{}.feather".format(output_dir, conversation_id))

            queue.task_done()
            done += 1
            bar.update(1)
    except Exception as e:
        print(e)
        a = 0


def do_crawl_from_fb(date_limit="01/01/2022", db_conn=None):
    print("Crawl from date {}".format(date_limit))
    conversation_ids = get_conversation_ids(date_limit)
    global bar
    bar = tqdm(total=len(conversation_ids))

    # print("Found {} conversations".format(len(conversation_ids)))
    if platform == "win32":
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())  # If run on windows
    asyncio.run(get_conversation_detail(conversation_ids, output_dir="chatlog9", from_date=date_limit, max_worker=16, db_conn=db_conn))
    bar.close()


if __name__ == '__main__':
    do_crawl_from_fb()
