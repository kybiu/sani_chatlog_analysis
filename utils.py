import pandas as pd

from datetime import datetime, timedelta
from typing import Text, Tuple

def is_week_change(cur_date:str,prev_date:str) -> bool:
    ''' Check whether 2 date is NOT in the same week
    '''
    if cur_date == None or prev_date == None:
        return False
    try:
        d1 = datetime.strptime(cur_date[:10],"%Y-%m-%d")
        d2 = datetime.strptime(prev_date[:10],"%Y-%m-%d")
        return not (d1.isocalendar()[1] == d2.isocalendar()[1] and d1.year == d2.year)
    except:
        return False

def get_start_end_week_date(cur_date:str) -> Tuple:
    ''' Get start/end date of the week given a date
    '''
    dt = datetime.strptime(cur_date[:10],"%Y-%m-%d")
    start = dt - timedelta(days=dt.weekday())
    end = start + timedelta(days=6)
    return start.strftime("%m-%d-%Y"),end.strftime("%m-%d-%Y")

        
def save_to_folder(path, week_messages_list):
    print(f'Saving to folder {path}')
    df = pd.DataFrame(week_messages_list)
    df.to_csv(path)