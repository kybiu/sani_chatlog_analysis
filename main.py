from crawl_chatlog import do_crawl_from_fb
from preprocess_chatlog import do_process_chatlog
from data_warehouse import do_update_datawarehouse
from datetime import datetime, timedelta
from configparser import ConfigParser
from sqlalchemy import create_engine
import pandas as pd

def config(filename='database.ini', section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)

    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return db


if __name__ == '__main__':
    # sqlalchemy connect
    params = config()
    conn_string = 'postgresql://{user}:{password}@{host}/{db}'.format(host=params["host"], user=params["user"], password=params["password"], db=params["database"])
    db_sqlalchemy = create_engine(conn_string)
    conn_sqlalchemy = db_sqlalchemy.connect()

    latest_date = pd.read_sql("""
        select full_date
        from dim_date
        order by full_date desc
        limit 1;
        """, conn_sqlalchemy)

    date_limit = latest_date["full_date"].dt.date.values[0].strftime('%d/%m/%Y')
    do_crawl_from_fb(date_limit=date_limit, db_conn=conn_sqlalchemy)
    do_process_chatlog(from_date=date_limit, db_conn=conn_sqlalchemy)
    do_update_datawarehouse(start_date=date_limit, db_conn=conn_sqlalchemy)

