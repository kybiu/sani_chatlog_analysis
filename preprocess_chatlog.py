import pandas as pd
from dotenv import load_dotenv
import os
import numpy as np
from sqlalchemy import create_engine
from configparser import ConfigParser
from datetime import datetime
from psycopg2.extensions import register_adapter, AsIs


def adapt_numpy_float64(numpy_float64):
    return AsIs(numpy_float64)


def adapt_numpy_int64(numpy_int64):
    return AsIs(numpy_int64)


load_dotenv()

ADMIN_ID = [int(x) for x in os.environ.get("ADMIN_ID").split(",")]
SANI_ID = int(os.environ.get("SANI_ID"))
DATE_LIMIT = os.environ.get("DATE_LIMIT")
bar = ...
register_adapter(np.float64, adapt_numpy_float64)
register_adapter(np.int64, adapt_numpy_int64)


def config(filename='database.ini', section='postgresql'):
    # create a parser
    parser = ConfigParser()
    # read config file
    parser.read(filename)
    # get section, default to postgresql
    db = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            db[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))
    return db


def connect_db():
    params = config()
    conn_string = 'postgresql://{user}:{password}@{host}/{db}'.format(user=params["user"], password=params["password"], host=params["host"], db=params["database"])
    db_sqlalchemy = create_engine(conn_string)
    return db_sqlalchemy


def get_data_from_db(start_date):
    db_sqlalchemy = connect_db()
    conn = db_sqlalchemy.connect()
    df = pd.read_sql("""
        select *
        from raw_chatlog_fb
        where created_time >=  \'{}\'
        """.format(start_date), conn)
    return df


def reformat_chatlog(df):
    # Drop duplicate
    df.drop_duplicates(subset=["message_id"], inplace=True)

    # Filter admin messages
    df = df[~df["from_id"].isin(ADMIN_ID)]
    df = df[~df["to_id"].isin(ADMIN_ID)]

    # Convert datetime column to datetime type
    df["updated_time"] = pd.to_datetime(df['updated_time'])
    df["created_time"] = pd.to_datetime(df['created_time'])

    # Add month/year col
    df["month/year"] = df["created_time"].apply(lambda x: "{month}/{year}".format(month=x.month, year=x.year))
    df["day"] = df["created_time"].apply(lambda x: x.day)
    df["month"] = df["created_time"].apply(lambda x: x.month)
    df["year"] = df["created_time"].apply(lambda x: x.year)

    df = df.replace(np.nan, '', regex=True)
    df = df[::-1]
    return df


def split_conversation_for_each_user(df, time_interval=3600, db_conn=None):
    df["conversation_no"] = [None] * len(df)
    try:
        conversation_id = df["conversation_id"].values[0]
        last_conversation = pd.read_sql("""
            select *
            from processed_chatlog
            where conversation_id = \'{}\'
            and conversation_no = (
                select max(conversation_no)
                from processed_chatlog
                where conversation_id = \'{}\')
            order by created_time desc
            limit 1;
            """.format(conversation_id, conversation_id), db_conn)

        df_numpy = df.to_numpy()

        if len(last_conversation) == 0:
            conv_no = 1
        else:
            conv_no = last_conversation["conversation_no"].values[0]
            time_diff_with_db = df_numpy[0][3] - pd.Timestamp(last_conversation["created_time"].values[0])
            if time_diff_with_db.total_seconds() >= time_interval:
                conv_no += 1

        for index, line in enumerate(df_numpy):
            message_id = line[1]
            created_time = line[3]
            df.loc[df.message_id == message_id, ['conversation_no']] = conv_no

            if index + 1 < len(df_numpy):
                next_line = df_numpy[index + 1]
                next_line_created_time = next_line[3]

                time_diff = next_line_created_time - created_time
                if time_diff.total_seconds() >= time_interval:
                    conv_no += 1
    except Exception as e:
        print(e)
    return df


def preprocess_chatlog(from_date="2022-01-01", db_conn=None):
    error_conversation_id = []

    from_date = datetime.strptime(from_date, "%d/%m/%Y").strftime('%Y-%m-%d')
    data = pd.read_sql("""
        select *
        from raw_chatlog_fb
        where created_time >=  \'{}\'
        """.format(from_date), db_conn)

    conversation_id_list = set(data["conversation_id"])
    for conv_id in conversation_id_list:
        df = data[data["conversation_id"] == conv_id]
        df = df.drop(columns=['index'])
        df["from_id"] = pd.to_numeric(df["from_id"])
        df_in_db = pd.read_sql("""
                select message_id
                from processed_chatlog
                where conversation_id = \'{}\'
                and created_time >= \'{}\'
            """.format(df["conversation_id"].values[0], from_date), db_conn)

        existed_message_id = df_in_db["message_id"].tolist()
        df = df[~(df["message_id"].isin(existed_message_id))]

        if len(df) == 0:
            continue
        else:
            time_interval = 3600
            df_reformatted = reformat_chatlog(df)
            if len(df_reformatted) > 0:
                df_split = split_conversation_for_each_user(df_reformatted, time_interval=time_interval, db_conn=db_conn)
                try:
                    if len(df_split) > 0:
                        df_split.to_sql('processed_chatlog', db_conn, if_exists='append')
                except Exception as e:
                    print(e)
            else:
                continue
    return error_conversation_id


def do_process_chatlog(from_date="2022-01-01", db_conn=None):
    print("Process conversation")
    preprocess_chatlog(from_date=from_date, db_conn=db_conn)


if __name__ == '__main__':
    data_directory = "chatlog9"
    output_dir = "processed_chatlog6"
    do_process_chatlog()
