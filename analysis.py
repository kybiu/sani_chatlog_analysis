import pandas as pd
from dotenv import load_dotenv
import os
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

pd.options.plotting.backend = "plotly"

load_dotenv()
ADMIN_ID = [int(x) for x in os.environ.get("ADMIN_ID").split(",")]
SANI_ID = int(os.environ.get("SANI_ID"))
PURPOSES = os.environ.get("PURPOSES").split(",")
LOCATIONS = os.environ.get("LOCATIONS").split(",")


def gen_userbymonth_fig(df):
    """
    Number of user send message by month
    Count distinct conversation id for each month
    :param df:
    """

    userbymonth_df = df.groupby(['month/year']).agg({"conversation_id": "nunique"})
    userbymonth_fig = userbymonth_df.plot.bar(labels=dict(value="Số user gửi tin nhắn"), text_auto=True)
    userbymonth_fig.update_layout(showlegend=False)
    userbymonth_fig.show()


def gen_messagebymonth_fig(df):
    """
    Number of users messages by month
    Count all the messages that sent by user for each month

    :param df: chatlog_df
    """

    df_without_message_from_sani = df[df["from_id"] != SANI_ID]
    messagebymonth_df = df_without_message_from_sani.groupby(['month/year']).agg({"message_id": "nunique"})
    messagebymonth_fig = messagebymonth_df.plot.bar(labels=dict(value="Số lượng tin nhắn sani nhận"), text_auto=True)
    messagebymonth_fig.update_layout(showlegend=False)

    messagebymonth_fig.show()


def gen_messsagebyuserbymonth_fig(df):
    """
    * user that have the longest conversation for each month
    * user that have the shortest conversation for each month
    * average number of message per conversation for each month
    :param df:
    """
    df_without_message_from_sani = df[df["from_id"] != SANI_ID]
    messagebyuserbymonth_df = df_without_message_from_sani.groupby(['month/year', "from_id"], as_index=False).agg({"message_id": "nunique"})
    messagebyuserbymonth_fig = px.box(messagebyuserbymonth_df,
                                      x="month/year",
                                      y="message_id",
                                      labels={"message_id": "Phân phối số lượng tin nhắn từng user mỗi tháng"})

    messagebyuserbymonth_fig.show()


def gen_messagebypurposeandlocation_fig(df):
    """
    Count message each purpose
    Purpose chosen the most

    :param df:
    """
    fig = make_subplots(rows=1, cols=2, specs=[[{"type": "pie"}, {"type": "pie"}]])
    titles = ["Số tin nhắn theo từng mục đích", "Số tin nhắn theo từng địa điểm"]
    df = df[df["from_id"] != SANI_ID]

    for idx, item in enumerate([PURPOSES, LOCATIONS]):
        item_df = df[df["content"].isin(item)]
        item_count_df = item_df["content"].value_counts()
        count_values = list(item_count_df.values)
        max_index = count_values.index(max(count_values))
        pull_part = [0] * len(item)
        pull_part[max_index] = 0.05
        colors = ['gold', 'mediumturquoise', 'darkorange', 'lightgreen', 'indigo', 'lightpink', 'lightseagreen']
        sub_fig = px.pie(pd.DataFrame(item_count_df).reset_index(), names="index", values="content")
        sub_fig.update_traces(title=titles[idx],
                              textinfo='percent+value+label',
                              pull=pull_part,
                              marker=dict(colors=colors, line=dict(color='#000000', width=1)),
                              showlegend=False)

        for d in sub_fig.data:
            fig.add_trace(d, row=1, col=idx + 1)

    fig.update_layout(font=dict(size=10))
    fig.show()


def gen_messageuserchatorchooseoption_fig(df):
    sani_replies_df = df[df["content"].str.contains("Một số quán")]
    sani_failed_df_1 = df[df["content"].str.contains("Sani chưa nhận ra được địa điểm mà bạn nói tới")]
    sani_failed_df_2 = df[df["content"].str.contains("Xin lỗi mình không tìm được quán cà phê phù hợp với yêu cầu của bạn mất rồi")]

    # thêm số lần thống kê bot im lặng bằng cách lấy thời gian tin nhắn của bot trừ đi tin nhắn user
    user_select_count = 0

    for row in sani_replies_df.iterrows():
        row_index = row[0]
        user_message = df.loc[[row_index + 1]]
        user_message_content = user_message["content"].values[0]
        if user_message_content in LOCATIONS:
            user_select_count += 1

    user_chat_count = len(sani_replies_df) - user_select_count
    colors1 = ['lightslategray', ] * 3
    colors1[0] = 'crimson'
    columns_fig1 = ['User chat', 'User sử dụng button', 'Sani đưa ra gợi ý thành công']
    values_fig1 = [user_chat_count, user_select_count, len(sani_replies_df), ]
    fig1 = go.Figure([go.Bar(x=columns_fig1, y=values_fig1, text=values_fig1, textposition='auto', marker_color=colors1)])

    colors2 = ['lightslategray', ] * 3
    colors2[2] = 'crimson'
    columns_fig2 = ['Không có kết quả phù hợp', 'Không nhận ra địa điểm', 'Đưa ra kết quả']
    values_fig2 = [len(sani_failed_df_2), len(sani_failed_df_1), len(sani_replies_df), ]
    fig2 = go.Figure([go.Bar(x=columns_fig2, y=values_fig2, text=values_fig2, textposition='auto', marker_color=colors2)])

    fig = make_subplots(rows=1, cols=2, specs=[[{"type": "bar"}, {"type": "bar"}]])

    fig.add_trace(
        go.Bar(x=columns_fig1, y=values_fig1, text=values_fig1, textposition='auto', marker_color=colors1),
        row=1, col=1
    )

    fig.add_trace(
        go.Bar(x=columns_fig2, y=values_fig2, text=values_fig2, textposition='auto', marker_color=colors2),
        row=1, col=2
    )
    fig.update_traces(showlegend=False)
    fig.show()


def gen_count_conv_by_month(df, last_msg_by_user_df):
    conv_byuserbymonth_df = df.groupby(['conversation_id', "month"]).agg({"conversation_no": "nunique"}).reset_index()
    conv_bymonth_df = conv_byuserbymonth_df.groupby(["month"]).agg({"conversation_id": "nunique", "conversation_no": "sum"}).reset_index().sort_values(['month', "conversation_no"],
                                                                                                                                                       ascending=[False,
                                                                                                                                                                  False]).reset_index(
        drop=True)
    conv_bymonth_df["month"] = conv_bymonth_df["month"].astype(str)
    conv_bymonth_df = conv_bymonth_df.rename(columns={"conversation_id": "Số lượng user", "conversation_no": "Số lượng conversation"})

    bot_silent_by_month_df = last_msg_by_user_df["month"].value_counts().reset_index()
    bot_silent_by_month_df = bot_silent_by_month_df.rename(columns={"index": "month", "month": "Số lượng conversation Sani không trả lời"})
    bot_silent_by_month_df["month"] = bot_silent_by_month_df["month"].astype(str)

    merge_df = pd.merge(conv_bymonth_df, bot_silent_by_month_df, on='month')
    fig = px.bar(merge_df.iloc[::-1],
                 x="month", y=["Số lượng user", "Số lượng conversation", "Số lượng conversation Sani không trả lời"],
                 title="Số conversation theo tháng", text_auto=True, barmode="group")
    # fig.update_traces(marker_color=["blue", 'green', 'red'])
    fig.update_layout(legend_title="")
    fig.show()


def gen_count_conv_by_day(df, last_msg_by_user_df):
    conv_byuserbyday_df = df.groupby(['conversation_id', "day", "month"]).agg({"conversation_no": "nunique"}).reset_index()
    conv_byday_df = conv_byuserbyday_df.groupby(["day", "month"]).agg({"conversation_id": "nunique", "conversation_no": "sum"}).reset_index().sort_values(['month', 'day'],
                                                                                                                                                          ascending=[True,
                                                                                                                                                                     True]).reset_index(
        drop=True)
    conv_byday_df["day/month"] = conv_byday_df["day"].astype(str) + "/" + conv_byday_df["month"].astype(str)
    conv_byday_df = conv_byday_df.rename(columns={"conversation_id": "Số lượng user", "conversation_no": "Số lượng conversation"})

    last_msg_by_user_df["day/month"] = last_msg_by_user_df["day"].astype(str) + "/" + last_msg_by_user_df["month"].astype(str)
    bot_silent_by_day_df = last_msg_by_user_df["day/month"].value_counts().reset_index()
    bot_silent_by_day_df = bot_silent_by_day_df.rename(columns={"index": "day/month", "day/month": "Số lượng conversation Sani không trả lời"})

    merge_df = pd.merge(conv_byday_df, bot_silent_by_day_df, on='day/month', how="outer").fillna(0)

    fig = px.bar(merge_df.iloc[::-1],
                 x="day/month",
                 y=["Số lượng conversation Sani không trả lời", "Số lượng conversation", "Số lượng user"],
                 title="Number of conversations by day",
                 text_auto=True, barmode="group",
                 height=800, )

    fig.update_layout(legend_title="")
    fig.update_traces(textangle=0, selector=dict(type='bar'))
    fig['layout']['xaxis']['autorange'] = "reversed"
    fig.show()


def gen_bot_silent(df, last_message_by_user_df):
    # bot_silent_april = last_message_by_user_df[last_message_by_user_df["month"] == 4]
    bot_silent_april = last_message_by_user_df[(last_message_by_user_df["month"] == 4) & (last_message_by_user_df["day"] >= 19)]
    bot_silent_message_count = bot_silent_april["last_user_message"].value_counts().reset_index()
    bot_silent_message_count = bot_silent_message_count[bot_silent_message_count["last_user_message"] > 4]
    bot_silent_message_count = bot_silent_message_count.rename(columns={"index": "Message cuối của user", "last_user_message": "Số lượng conversation Sani không trả lời"})
    fig = px.bar(bot_silent_message_count.iloc[::-1],
                 x="Message cuối của user",
                 y="Số lượng conversation Sani không trả lời",
                 text_auto=True, barmode="group",
                 height=800)
    fig.show()


def check_userorbotsilent_case(df):
    bot_silent_df_list = []
    bot_silent_lastmessage_df = pd.DataFrame(columns=['conversation_id', 'conversation_no', 'last_bot_message', 'last_user_message', "day", "month"])
    bot_silent_lastmessage_df_list = []
    user_silent_df_list = []
    user_silent_lastmessage_df = pd.DataFrame(columns=['conversation_id', 'conversation_no', 'last_bot_message', 'last_user_message', "day", "month"])
    complete_conversation_df_list = []

    for conv_id in set(df["conversation_id"]):
        user_conversations_df = df[df["conversation_id"] == conv_id]
        conversations_no = set(user_conversations_df["conversation_no"])
        for conv_no in conversations_no:
            conv_df = user_conversations_df[user_conversations_df["conversation_no"] == conv_no]
            last_message_from = conv_df.iloc[-1]["from_name"]

            if "Sani" in last_message_from:
                user_silent_df_list.append(conv_df)
            else:
                last_message_of_sani = conv_df[conv_df["from_name"].str.contains("Sani") & conv_df['content'].notna()]

                if len(last_message_of_sani) > 0:
                    last_message_of_sani = last_message_of_sani.iloc[-1]["content"]
                else:
                    last_message_of_sani = ""

                if "Một số quán để" in last_message_of_sani:
                    last_message_of_sani = "Một số quán để"
                elif "Bạn muốn xem thêm" in last_message_of_sani:
                    last_message_of_sani = "Bạn muốn xem thêm"
                elif "Đây là một số ảnh" in last_message_of_sani or "Bạn coi ảnh" in last_message_of_sani:
                    last_message_of_sani = "Đây là một số ảnh"
                elif "Sani gửi bạn đánh giá" in last_message_of_sani:
                    last_message_of_sani = "Sani gửi bạn đánh giá"
                elif "Đây là link" in last_message_of_sani:
                    last_message_of_sani = "Đây là link"
                elif "Sani đang tạm ngừng hoạt động" in last_message_of_sani:
                    last_message_of_sani = "Sani đang tạm ngừng hoạt động"
                elif "Đây là 1 số quán" in last_message_of_sani:
                    last_message_of_sani = "Đây là 1 số quán"

                last_message = conv_df.iloc[-1]["content"]
                if not isinstance(last_message, str):
                    last_message = "send icon"

                if "tìm quán gần" in last_message.lower():
                    last_message = "Tìm quán gần"
                elif "tìm quán" in last_message.lower():
                    last_message = "Tìm quán"
                elif any(text in last_message.lower() for text in ["get started", "bắt đầu", "hi", "alo", "lô", "start", "démarrer"]):
                    last_message = "start"
                elif any(text in last_message.lower() for text in ["/restart", "restart"]):
                    last_message = "restart"
                elif any(text in last_message.lower() for text in ["cảm ơn", "thank", "ok rùi đó"]):
                    last_message = "cảm ơn"

                new_row = {'conversation_id': conv_id,
                           'conversation_no': conv_no,
                           'last_bot_message': last_message_of_sani,
                           'last_user_message': last_message,
                           'day': conv_df.iloc[-1]["day"],
                           'month': conv_df.iloc[-1]["month"]}

                bot_silent_lastmessage_df_list.append(new_row)
                # bot_silent_lastmessage_df = bot_silent_lastmessage_df.append(new_row, ignore_index=True)
                bot_silent_df_list.append(conv_df)

    bot_silent_lastmessage_df = pd.DataFrame(bot_silent_lastmessage_df_list)
    user_silent_df = pd.concat(user_silent_df_list)
    bot_silent_df = pd.concat(bot_silent_df_list)

    return bot_silent_lastmessage_df


def user_turnover(df):
    a = df.copy()
    number_user_turnover = []
    number_user_in_month = []
    old_user = set()
    for index, month in enumerate(set(a["month"])):
        a["from_id"] = pd.to_numeric(a["from_id"])
        user_in_month = set(a[a["month"] == month]["conversation_id"])
        if index != 0:
            new_user = user_in_month.difference(old_user)
            # t_1002913027317226
            number_user_in_month.append(len(user_in_month))
            number_user_turnover.append(len(user_in_month) - len(new_user))

        old_user.update(user_in_month)

    months = list(set(a["month"]))[1:]
    fig = go.Figure(data=[
        go.Bar(name='Số user quay lại nhắn với Sani', x=months, y=number_user_turnover, text=number_user_turnover, textposition='auto'),
        go.Bar(name='Tổng số user', x=months, y=number_user_in_month, text=number_user_in_month, textposition='auto')
    ])
    fig.update_layout(barmode='group')
    fig.update_layout(
        title="Tỷ lệ khách quay lại với Sani",
        xaxis_title="Tháng",
        yaxis_title="Số lượng user",
    )
    fig.show()
