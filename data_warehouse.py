import json
from configparser import ConfigParser
import pandas as pd
import psycopg2
from sqlalchemy import create_engine
import numpy as np
from analysis import check_userorbotsilent_case
from datetime import datetime, timedelta


def update_dim_conv(chatlog_df, last_msg_by_user_df, start_date, conn_sqlalchemy):
    print("Update dim_convo")
    # Count message of sani and user
    count_user_message_df = chatlog_df.loc[chatlog_df.from_id != 103250421878333].groupby(["conversation_id", "conversation_no"]).agg(
        {"message_id": "nunique", "created_time": "first", "from_id": "first"}).reset_index()
    count_user_message_df.rename(columns={'message_id': 'no_messages_user'}, inplace=True)
    count_bot_message_df = chatlog_df.loc[chatlog_df.from_id == 103250421878333].groupby(["conversation_id", "conversation_no"]).agg(
        {"message_id": "nunique", "created_time": "first", "from_id": "first"}).reset_index()
    count_bot_message_df.rename(columns={'message_id': 'no_messages_sani'}, inplace=True)

    # merge 2 df of sani message count and user message count
    dim_convo_df = pd.merge(count_user_message_df, count_bot_message_df, on=["conversation_id", "conversation_no"], how="outer", suffixes=('', '_'))
    dim_convo_df["from_id"] = dim_convo_df['from_id'].fillna(dim_convo_df['from_id_'])
    dim_convo_df["created_time"] = dim_convo_df['created_time'].fillna(dim_convo_df['created_time_'])
    dim_convo_df.pop("from_id_")
    dim_convo_df.pop("created_time_")
    dim_convo_df[["no_messages_user", "no_messages_sani"]] = dim_convo_df[["no_messages_user", "no_messages_sani"]].fillna(0)
    dim_convo_df["total_message"] = dim_convo_df["no_messages_user"] + dim_convo_df["no_messages_sani"]

    # Get conversation that is silent
    last_msg_by_user_df = last_msg_by_user_df.dropna(subset=["last_user_message"])
    last_msg_by_user_df["silent"] = [1] * len(last_msg_by_user_df)
    last_msg_by_user_df = last_msg_by_user_df[["conversation_id", "conversation_no", "silent"]]

    # merge count message and silent message
    dim_convo_df = pd.merge(dim_convo_df, last_msg_by_user_df, on=["conversation_id", "conversation_no"], how="outer")

    # drop duplicate and insert to database
    dim_convo_df.drop_duplicates(subset=["conversation_id", "conversation_no"], inplace=True)
    dim_convo_df.fillna(0, inplace=True)
    dim_convo_df.rename(columns={'conversation_id': 'conv_id', "conversation_no": "conv_no"}, inplace=True)

    dim_conv_in_db = pd.read_sql("""
           select *
           from dim_convo
           where created_time >=  \'{}\'
           """.format(start_date), conn_sqlalchemy)

    dim_conv_in_db["conv_id_created_time"] = dim_conv_in_db["conv_id"] + "_" + dim_conv_in_db["created_time"].apply(lambda x: str(x))
    dim_convo_df["conv_id_created_time"] = dim_convo_df["conv_id"] + "_" + dim_convo_df["created_time"].apply(lambda x: str(x))

    dim_conv_in_db["conv_id_conv_no"] = dim_conv_in_db["conv_id"] + "_" + dim_conv_in_db["conv_no"].apply(lambda x: str(x))
    dim_convo_df["conv_id_conv_no"] = dim_convo_df["conv_id"] + "_" + dim_convo_df["conv_no"].apply(lambda x: str(x))

    existed_conv_id_created_time = dim_conv_in_db["conv_id_created_time"].tolist()
    existed_conv_id_conv_no = dim_conv_in_db["conv_id_conv_no"].tolist()

    # Remove row that have same conv_id and created_time as in db
    dim_convo_df = dim_convo_df[~(dim_convo_df["conv_id_created_time"].isin(existed_conv_id_created_time))]

    # Remove row that have same conv_id and conv_no as in db
    dim_convo_df1 = dim_convo_df[~(dim_convo_df["conv_id_conv_no"].isin(existed_conv_id_conv_no))]
    if len(dim_convo_df1) > 0:
        # Insert new conversation to dim_convo
        dim_convo_df1.drop('conv_id_created_time', inplace=True, axis=1)
        dim_convo_df1.drop('conv_id_conv_no', inplace=True, axis=1)
        dim_convo_df1.to_sql('dim_convo', conn_sqlalchemy, if_exists='append')

    # Update table dim_convo
    # dim_convo_df2 contains row that have same conv_id and conv_no in db
    dim_convo_df2 = dim_convo_df[dim_convo_df["conv_id_conv_no"].isin(existed_conv_id_conv_no)].reset_index(drop=True)
    conv_id_to_update = dim_convo_df2["conv_id_conv_no"].tolist()

    # dim_conv_in_db_to_update contains rows need to update in db
    dim_conv_in_db_to_update = dim_conv_in_db[dim_conv_in_db["conv_id_conv_no"].isin(conv_id_to_update)]
    new_no_messages_user = dim_convo_df2["no_messages_user"].apply(lambda x: int(x)).reset_index(drop=True) + dim_conv_in_db_to_update["no_messages_user"].apply(
        lambda x: int(x)).reset_index(drop=True)
    new_no_messages_sani = dim_convo_df2["no_messages_sani"].apply(lambda x: int(x)).reset_index(drop=True) + dim_conv_in_db_to_update["no_messages_sani"].apply(
        lambda x: int(x)).reset_index(drop=True)
    new_total_message = dim_convo_df2["total_message"].apply(lambda x: int(x)).reset_index(drop=True) + dim_conv_in_db_to_update["total_message"].apply(
        lambda x: int(x)).reset_index(drop=True)

    dim_convo_df2["no_messages_user"] = new_no_messages_user
    dim_convo_df2["no_messages_sani"] = new_no_messages_sani
    dim_convo_df2["total_message"] = new_total_message

    dim_convo_df2.drop('conv_id_created_time', inplace=True, axis=1)
    dim_convo_df2.drop('conv_id_conv_no', inplace=True, axis=1)
    for item in dim_conv_in_db_to_update.to_numpy():
        conversation_id = item[1]
        conversation_no = item[2]
        conn_sqlalchemy.execute("DELETE FROM dim_convo WHERE (conv_id = \'{}\' and conv_no={})".format(conversation_id, conversation_no))

    dim_convo_df2.to_sql('dim_convo', conn_sqlalchemy, if_exists='append')

    return dim_convo_df1.append(dim_convo_df2)


def update_dim_user(chatlog_df, conn_sqlalchemy):
    print("Update dim_user")
    dim_user_df = chatlog_df[["from_id", "from_name", "month/year", "conversation_no"]][chatlog_df["from_id"] != 103250421878333]
    temp_list = []
    dim_user_df_dropduplicate = dim_user_df.drop_duplicates(subset=["conversation_no"])

    for id in set(dim_user_df["from_id"]):
        user_df = dim_user_df[dim_user_df["from_id"] == id]
        user_df.drop_duplicates(subset=["conversation_no"], inplace=True)
        month_start = user_df[user_df["conversation_no"] == user_df["conversation_no"].min()]["month/year"].values[0]
        month_return = list(set(user_df[user_df["conversation_no"] > 1]["month/year"].values))
        if month_start in month_return:
            month_return.remove(month_start)
        month_return = ",".join(month_return)
        temp_list.append((id, month_start, month_return))

    dim_user_df_complementaire = pd.DataFrame(temp_list, columns=["user_id", "month_start", "month_return"])
    dim_user_df = dim_user_df.groupby(dim_user_df["from_id"], as_index=False).agg({"from_name": "first", "month/year": "nunique", "conversation_no": "nunique"})
    dim_user_df['no_return_month'] = dim_user_df.apply(lambda row: row["month/year"] - 1 if (row["month/year"] < row["conversation_no"]) else row["conversation_no"] - 1,
                                                       axis=1)
    dim_user_df = dim_user_df[["from_id", "from_name", "no_return_month"]]
    dim_user_df.rename(columns={'from_id': 'user_id', 'from_name': 'user_name'}, inplace=True)

    dim_user_df_2 = pd.merge(dim_user_df, dim_user_df_complementaire, on='user_id')
    new_user = []
    for item in dim_user_df_2.to_numpy():
        user_id = item[0]
        user_existed_df = pd.read_sql("""
            select *
            from dim_user
            where user_id =  {}
            """.format(user_id), conn_sqlalchemy)

        if len(user_existed_df) == 0:
            new_user.append(user_id)
        else:
            # Update table
            user_existed_df.at[0, "no_return_month"] = user_existed_df.at[0, "no_return_month"] + 1
            if len(user_existed_df.at[0, "month_return"]) == 0:
                user_existed_df.at[0, "month_return"] = item[3]
            else:
                user_existed_df.at[0, "month_return"] = user_existed_df.at[0, "month_return"] + "," + item[3]

            # delete old row
            conn_sqlalchemy.execute("DELETE FROM dim_user WHERE user_id = {} ".format(user_id))

            # insert row after updated
            user_existed_df.drop('index', inplace=True, axis=1)

            user_existed_df.to_sql('dim_user', conn_sqlalchemy, if_exists='append')

    # Insert new to table
    new_user_df = dim_user_df_2[dim_user_df_2["user_id"].isin(new_user)]
    new_user_df.to_sql('dim_user', conn_sqlalchemy, if_exists='append')


def update_fact(dim_convo_df, conn_sqlalchemy):
    print("Update fact")
    conversation_id_list = set(dim_convo_df["conv_id"])
    fact_df_list = []
    for conversation_id in conversation_id_list:
        convo_df_from_db = pd.read_sql("""
            select *
            from dim_convo
            where conv_id =  \'{}\'
            """.format(conversation_id), conn_sqlalchemy)
        convo_df_from_db["date"] = pd.to_datetime(convo_df_from_db["created_time"]).dt.strftime('%d/%m/%Y').reset_index(drop=True)
        fact_df = convo_df_from_db.groupby(["conv_id", "date"]).agg(
            {"from_id": "first", "silent": "sum", "conv_no": lambda x: list(x), "no_messages_user": "sum", "no_messages_sani": "sum"}).reset_index()
        fact_df["no_conv"] = fact_df["conv_no"].apply(lambda x: len(x))
        fact_df["total_no_message"] = fact_df["no_messages_user"] + fact_df["no_messages_sani"]
        fact_df.rename(columns={'from_id': 'user_id', "silent": "no_silent", "conv_no": "conv_no_list"}, inplace=True)
        fact_df["no_resp"] = fact_df["no_conv"] - fact_df["no_silent"]
        fact_df[["user_id", "no_silent", "no_resp", "no_messages_user", "no_messages_sani", "no_conv", "total_no_message"]] = fact_df[
            ["user_id", "no_silent", "no_resp", "no_messages_user", "no_messages_sani", "no_conv", "total_no_message"]].astype(np.int64)
        fact_df["date"] = pd.to_datetime(fact_df["date"], format='%d/%m/%Y')

        conn_sqlalchemy.execute("DELETE FROM fact WHERE conv_id = \'{}\' ".format(conversation_id))
        fact_df.to_sql('fact', conn_sqlalchemy, if_exists='append')
        fact_df_list.append(fact_df)
    if len(fact_df_list) != 0:
        all_fact_df = pd.concat(fact_df_list)
    else:
        all_fact_df = None
    return all_fact_df


def update_dim_date(fact_df, conn_sqlalchemy):
    print("Update dim_date")
    new_date_list = []
    for item in fact_df.to_numpy():
        date = item[1]
        date_df_from_db = pd.read_sql("""
            select *
            from dim_date
            where full_date =  \'{}\'
            """.format(date), conn_sqlalchemy)
        if len(date_df_from_db) == 0:
            new_date_list.append(date)

    dim_date_df = fact_df[fact_df["date"].isin(new_date_list)][["date"]]
    dim_date_df.rename(columns={'date': 'full_date'}, inplace=True)
    dim_date_df.drop_duplicates(subset=["full_date"], inplace=True)
    dim_date_df["date"] = dim_date_df["full_date"].apply(lambda x: x.day)
    dim_date_df["no_day_week"] = dim_date_df["full_date"].dt.weekday
    # dim_date_df["week"] = dim_date_df["full_date"].dt.isocalendar().week
    dim_date_df["month"] = dim_date_df["full_date"].dt.month
    dim_date_df["year"] = dim_date_df["full_date"].dt.year
    dim_date_df["no_semest"] = np.where(dim_date_df.full_date.dt.quarter.gt(2), 2, 1)
    dim_date_df["no_trimest"] = dim_date_df["full_date"].dt.quarter
    dim_date_df.to_sql('dim_date', conn_sqlalchemy, if_exists='append')


def connect(chatlog_df, last_msg_by_user_df, start_date, conn_sqlalchemy=None):
    """ Connect to the PostgreSQL database server """
    chatlog_df = chatlog_df.sort_values(["conversation_id", "created_time"])

    # ----- UPSERT TO DIM_CONVO
    dim_convo_df = update_dim_conv(chatlog_df, last_msg_by_user_df, start_date, conn_sqlalchemy)

    # ------ INSERT TO DIM_USER
    update_dim_user(chatlog_df, conn_sqlalchemy)

    # ----- INSERT TO FACT
    fact_df = update_fact(dim_convo_df, conn_sqlalchemy)

    # ----- INSERT TO DIM_DATE
    update_dim_date(fact_df, conn_sqlalchemy)


def do_update_datawarehouse(start_date="2022-01-01", db_conn=None):
    start_date = datetime.strptime(start_date, "%d/%m/%Y").strftime('%Y-%m-%d')

    chatlog_df = pd.read_sql("""
        select *
        from processed_chatlog
        where created_time >=  \'{}\'
        """.format(start_date), db_conn)

    last_msg_by_user_df = check_userorbotsilent_case(chatlog_df)
    connect(chatlog_df, last_msg_by_user_df, start_date, conn_sqlalchemy=db_conn)
